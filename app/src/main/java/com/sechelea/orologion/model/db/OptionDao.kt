package com.sechelea.orologion.model.db

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface OptionDao {
    @Query("select * from option")
    fun getOptions() : Flow<List<Option>>

    @Query("select value from option where name = :name")
    fun getOption(name: String) : Flow<String?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(option: Option)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertIfNotExists(option: Option)

    @Update
    suspend fun update(option: Option) : Int
}