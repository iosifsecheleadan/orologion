package com.sechelea.orologion.model.repo

import com.sechelea.orologion.model.db.Option
import com.sechelea.orologion.model.db.OptionDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter

class OptionRepository(
    private val optionDao: OptionDao
) {
    private val daysOfWeek = listOf("mon", "tue", "wed", "thu", "fri", "sat", "sun")
    private val formatPattern = DateTimeFormatter.ofPattern("yyyy-MM-dd")

    private fun getDate(date : LocalDate = LocalDate.now()) : String =
        date.format(formatPattern)

    private fun getDayOfWeek(date: LocalDate = LocalDate.now()) : String =
        date.dayOfWeek.name.substring(0, 3).toLowerCase()

    private fun isNight(date: LocalTime = LocalTime.now()) : Boolean =
        date.hour !in 6..20

    suspend fun setDefaultOptions() {
        optionDao.insert(Option("else", "true"))

        // only write these if they don't exist
        optionDao.insertIfNotExists(Option("theme", "paper"))
        optionDao.insertIfNotExists(Option("lang", "Romana"))

        optionDao.insertIfNotExists(Option("is_priest_present", "false"))
        optionDao.insertIfNotExists(Option("is_long", "false"))

        // overwrite these every time
        setDate()
    }

    fun theme() = option("theme")
    suspend fun setTheme(string: String) = setOption("theme", string)

    fun language() = option("lang")
    suspend fun setLanguage(string: String) = setOption("lang", string)

    fun date() = option("date")
    /** Sets options date, is_night and days of week */
    suspend fun setDate(date: LocalDate = LocalDate.now()) {
        setOption("date", getDate(date))
        setDayOfWeek(date)
        setOption("is_night", isNight().toString())
    }

    /** Assumes date and days of week correspond */
    fun dayOfWeek() = date().map { getDayOfWeek(
        // turns out room returns null instead of empty string
        LocalDate.parse(it ?: getDate(), formatPattern)
    ) }
    /** Sets options mon, tue, wed, thu, fri, sat, sun */
    private suspend fun setDayOfWeek(date: LocalDate = LocalDate.now()) {
        for (day in daysOfWeek) setOption(day, "false")
        setOption(getDayOfWeek(date), "true")
    }


    fun isPriestPresent() = option("is_priest_present").map { it == "true" }
    suspend fun setIsPriestPresent(boolean: Boolean) {
        setOption("is_priest_present", boolean.toString())
    }

    fun isLong() = option("is_long").map { it == "true" }
    suspend fun setIsLong(boolean: Boolean) = setOption("is_long", boolean.toString())

    fun option(option: String): Flow<String?> = optionDao.getOption(option)
    private suspend fun setOption(name: String, value: String) = setOption(Option(name, value))
    private suspend fun setOption(option: Option) = optionDao.insert(option)
}