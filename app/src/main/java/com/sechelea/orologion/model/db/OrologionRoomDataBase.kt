package com.sechelea.orologion.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(version = 1, exportSchema = false, entities = [
    Option::class
])
abstract class OrologionRoomDataBase : RoomDatabase() {

    abstract fun optionDao() : OptionDao

    companion object {
        @Volatile
        private var INSTANCE: OrologionRoomDataBase? = null

        fun getDataBase(context: Context): OrologionRoomDataBase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    OrologionRoomDataBase::class.java,
                    "orologion_database"
                ).fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}