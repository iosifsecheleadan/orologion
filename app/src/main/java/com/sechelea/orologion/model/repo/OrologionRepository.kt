package com.sechelea.orologion.model.repo

import android.content.res.AssetManager
import android.util.Log
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import java.lang.Exception
import kotlin.collections.ArrayList

class OrologionRepository(
    private val assets: AssetManager,
    private val optionRepo: OptionRepository
) {
    companion object {
        private const val DELAY_MS : Long = 1000

        // private const val TEXTS = "texts-"
        // private const val TEXTS = ""
        private const val COMMON = "0. common"
        private const val EXTENSION = ".txt"
        private const val SERVICE = "_SERVICE"
        private const val COUNT = "count.txt"

        private const val IF = "if"
        private const val END_IF = "endif"
        private const val REDIRECT = "@"
        private const val COMMENT = "//"
    }

    private var language: String? = null
    private var service: String? = null

    private fun languageRoot()                  = "$language"
    private fun commonPrayer(prayer: String)    = "$language/$COMMON/$prayer$EXTENSION"
    private fun servicePrayer(prayer: String)   = "$language/$service/$prayer$EXTENSION"
    private fun serviceRoot()                   = "$language/$service/$SERVICE$EXTENSION"
    private fun countLocation()                 = "$language/$COUNT"


    fun useLanguage(language: String) {
        this.language = language
    }

    fun allLanguages() = flow {
        emit(getLanguages())
        while (true) {
            delay(DELAY_MS)
            emit(getLanguages())
        }
    }

    private fun getLanguages(): List<String> {
        return try {
            assets.list("")?.filter {
                ! it.endsWith(EXTENSION) && it[0].isUpperCase()
            } ?: listOf("No data")
        } catch (e : Exception) {
            Log.d(e.message, e.stackTraceToString())
            listOf("Loading ...")
        }
    }

    fun allServices(language: String) = flow {
        emit(getServices(language))
        while (true) {
            delay(DELAY_MS)
            emit(getServices(language))
        }
    }

    private fun getServices(language: String): List<String> {
        this.language = language
        return try {
            assets.list(languageRoot())?.filter {
                ! it.endsWith(EXTENSION)
            } ?: listOf("No data")
        } catch (e : Exception) {
            Log.d(e.message, e.stackTraceToString())
            listOf("Loading ...")
        }
    }

    fun serviceText(service: String) = flow {
        emit(parseService(service))
        while (true) {
            delay(DELAY_MS)
            emit(parseService(service))
        }
    }

    private suspend fun parseService(service: String): List<List<String>> {
        this.service = service
        return try {
            this.parseFile(serviceRoot())
                .reduce { first, second -> first + "\n" + second }
                .split("\n \n")
                .map { it.split("\n")
                    .filter { string -> string.isNotBlank() } }
                .filter { it.isNotEmpty() }
        } catch (e: Exception) {
            Log.d(e.message, e.stackTraceToString())
            listOf(listOf("Loading ..."))
        }
    }

    private suspend fun parseFile(fileName: String): ArrayList<String> {
        val result = arrayListOf<String>()
        val lines = assets.open(fileName).bufferedReader().readLines() as MutableList<String>
        while (lines.isNotEmpty()) {
            var current = lines.removeAt(0).trim()
            if (current == IF) {
                val ifLines = arrayListOf<String>()
                current = lines.removeAt(0).trim()
                while (current != END_IF) {
                    ifLines.add(current)
                    current = lines.removeAt(0).trim()
                }
                result.addAll(parseIf(ifLines))
            } else result.addAll(parseLine(current))
        }
        return result

    }

    private suspend fun parseIf(ifLines: ArrayList<String>): List<String> {
        for (line: String in ifLines) {
            val splitt = line.split(" ") as MutableList
            val condition = splitt.removeAt(0)
            if (optionRepo.option(condition).first() == "true") {
                return parseLine(splitt.joinToString(" "))
            }
        }
        return emptyList()
    }

    private suspend fun parseLine(line: String): List<String> {
        return  if (line.startsWith(REDIRECT)) {
                    val splitt = line.split(" ")
                    if (splitt.size > 1) {
                        getPrayer(splitt[0].substring(1), splitt[1].toInt())
                    } else getPrayer(line.substring(1))
                } else if (line.startsWith(COMMENT)) emptyList()
                else if (line.isEmpty()) listOf(" ")
                else listOf(line)
    }

    private suspend fun getPrayer(prayer: String, count: Int = 1): List<String> {
        val result = arrayListOf<String>()
        if (count > 1) result.add(getRepeat(count))

        try{
            result.addAll(parseFile(commonPrayer(prayer)))
            return result
        } catch (e : Exception) {}

        try {
            result.addAll(parseFile(servicePrayer(prayer)))
            return result
        } catch (e: Exception) {}

        result.add("Couldn't find prayer \"$prayer\". (${commonPrayer(prayer)}; ${servicePrayer(prayer)})")
        return result
    }

    private fun getRepeat(count: Int): String {
        return assets.open(countLocation()).bufferedReader().readLine()
            .replace("X", count.toString())
    }
}