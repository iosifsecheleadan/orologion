package com.sechelea.orologion.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Option (
    @PrimaryKey var name: String,
    var value: String,
){

}