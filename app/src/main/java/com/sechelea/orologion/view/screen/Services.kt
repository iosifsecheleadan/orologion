package com.sechelea.orologion.view.screen

import android.util.Log
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import com.sechelea.orologion.view.custom.*
import com.sechelea.orologion.viewmodel.OrologionViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class Services(
    navController: NavHostController,
    private val viewModel: OrologionViewModel
) : Screen(navController) {

    @Composable
    override fun Show() {
        val scaffoldState = rememberScaffoldState(rememberDrawerState(initialValue = DrawerValue.Closed))
        val scaffoldScope = rememberCoroutineScope()
        Scaffold(
            topBar = { TopBar(scaffoldState, scaffoldScope, "All Services") },
            drawerContent = { SideDrawer(scaffoldState, scaffoldScope, navController) },
            scaffoldState = scaffoldState,
        ) {
            Content()
        }
    }

    @Composable
    private fun Content() {
        val language by viewModel.optionRepo.language().collectAsState(initial = "Language")
        val services by viewModel.orologionRepo.allServices(language.toString()).collectAsState(initial = emptyList())

        WideCard { WideColumn {
            for (service in services) {
                WideButton(onClick = {
                    Log.d("NAV", "service/$service")
                    navController.navigate("service/$service")
                }) {
                    Text(text = service.toString())
                }
            }
        } }
    }
}
