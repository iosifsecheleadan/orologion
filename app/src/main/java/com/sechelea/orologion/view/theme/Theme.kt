package com.sechelea.orologion.view.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.material.Colors
import androidx.compose.ui.graphics.Color
import java.lang.IllegalArgumentException


// https://material.io/resources/color/#!/?view.left=0&view.right=1&primary.color=B71C1C&secondary.color=d32f2f
val primary         = Color(0xFF_b71c1c)
val primaryLight    = Color(0xFF_f05545)
val primaryDark     = Color(0xFF_7f0000)

val secondary       = Color(0xFF_d32f2f)
val secondaryLight  = Color(0xFF_ff6659)
val secondaryDark   = Color(0xFF_9a0007)

// https://www.schemecolor.com/sample?getcolor=e0d3af
val paperDark       = Color(0xFF_e0d3af)
val paperLight      = Color(0xFF_e9e0cf) // a little lighter than below
// val paperLight      = Color(0xFF_e6dcbf) // a little darker tha above

// Light colors with Desert Tan Background and Red accent
private val PaperColorPalette = lightColors(
    primary = primary,
    primaryVariant = primaryLight,
    secondary = secondary,
    secondaryVariant = secondaryLight,
    background = paperDark,
    surface = paperLight,

    /*
    surface = ???,
     */
)

// Light colors with White background and Red accent
private val LightColorPalette = lightColors(
    primary = primary,
    primaryVariant = primaryLight,
    secondary = secondary,
    secondaryVariant = secondaryLight,

    /*
    surface = ???,
     */
)

// Dark colors with Black background and Red accent
private val DarkColorPalette = darkColors(
    primary = primary,
    primaryVariant = primaryDark,
    secondary = secondary,
    secondaryVariant = secondaryDark,

    /*
    surface = ???,
     */
)

enum class Themes(val colors: Colors) {
    Paper   (PaperColorPalette),
    Light   (LightColorPalette),
    Dark    (DarkColorPalette)
}

@Composable
fun OrologionTheme(
    theme: String?,
    content: @Composable() () -> Unit
) {
    val colors = try {
        Themes.valueOf(theme?.capitalize() ?: "Paper").colors
    } catch (e : IllegalArgumentException) { PaperColorPalette }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}