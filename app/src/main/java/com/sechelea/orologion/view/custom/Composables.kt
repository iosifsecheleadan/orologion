package com.sechelea.orologion.view.custom

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.navigate
import com.sechelea.orologion.view.screen.SideNavDrawerItems
import com.sechelea.orologion.view.theme.OrologionTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
fun ThemedBackground(
    theme: String?,
    content: @Composable () -> Unit
) {
    OrologionTheme(theme) {
        Surface(color = MaterialTheme.colors.background) {
            content()
        }
    }
}

@Composable
fun WideColumn(
    content: @Composable() () -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        content()
    }
}

@Composable
fun ScrollableWideColumn(
    content: @Composable() () -> Unit
) {
    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        content()
    }
}

@Composable
fun WideRow(
    horizontalArrangement: Arrangement.Horizontal = Arrangement.SpaceBetween,
    content: @Composable () -> Unit
) {
    Row(
        horizontalArrangement = horizontalArrangement,
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        content()
    }
}

@Composable
fun WideButton(
    onClick: () -> Unit,
    colors: ButtonColors = ButtonDefaults.buttonColors(),
    content: @Composable() () -> Unit
) {
    Button(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        colors = colors,
        onClick = onClick
    ) {
        content()
    }
}

@Composable
fun WideText(
    text: String,
) {
    Text(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        text = text
    )
}

@Composable
fun WideColoredText(
    text: String,
    color: Color = Color.Unspecified,
    style: TextStyle = LocalTextStyle.current,
    fontStyle: FontStyle? = null,
) {
    Text(
        color = color,
        textAlign = TextAlign.Justify,
        modifier = Modifier
            .fillMaxWidth(),
        text = text,
        style = style,
        fontStyle = fontStyle
    )

}

@Composable
fun WideCard(
    content: @Composable () -> Unit
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        content()
    }

}


@Composable
fun TopBar(state: ScaffoldState, scope: CoroutineScope, title: String) {
    TopAppBar(
        title = { Text(text = title) },
        navigationIcon = {
            IconButton(onClick = { scope.launch {
                state.drawerState.open()
            } }) { Icon(imageVector = Icons.Default.Menu, contentDescription = null) }
        }
    )
}

@Composable
fun SideDrawer(state: ScaffoldState, scope: CoroutineScope, navController: NavController) {
    Spacer(modifier = Modifier.height(100.dp))

    for (item in SideNavDrawerItems.values()) {
        WideButton(onClick = {
            scope.launch { state.drawerState.close() }
            navController.navigate(item.name.toLowerCase())
        }) {
            WideRow {
                Icon(imageVector = item.icon, contentDescription = null)
                Text(text = item.name)
            }
        }
    }
}