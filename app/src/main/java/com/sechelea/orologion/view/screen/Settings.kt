package com.sechelea.orologion.view.screen

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.*
import androidx.navigation.NavController
import androidx.navigation.compose.navigate
import com.sechelea.orologion.view.custom.*
import com.sechelea.orologion.view.theme.Themes
import com.sechelea.orologion.viewmodel.OrologionViewModel
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.buttons
import com.vanpra.composematerialdialogs.datetime.datepicker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


class Settings(
    navController: NavController,
    private val viewModel: OrologionViewModel
) : Screen(navController) {

    @Composable
    override fun Show() {
        val scaffoldState = rememberScaffoldState(rememberDrawerState(initialValue = DrawerValue.Closed))
        val scaffoldScope = rememberCoroutineScope()
        Scaffold(
            topBar = { TopBar(scaffoldState, scaffoldScope, "Settings") },
            drawerContent = { SideDrawer(scaffoldState, scaffoldScope, navController) },
            scaffoldState = scaffoldState,
        ) {
            Content()
        }
    }

    @Composable
    private fun Content() {
        ScrollableWideColumn {
            WideCard { WideColumn {
                LongOrShortSetting()
                // IsPriestSettings()
                IsPriestPresentSettings()
            } }
            WideCard { WideColumn {
                DateSetting()
                LanguageSettings()
                ThemeSettings()
            } }
        }
    }

    @Composable
    private fun LongOrShortSetting() {
        WideRow {
            Text(text = "Use long form prayers")

            val isLong by viewModel.optionRepo.isLong().collectAsState(initial = false)
            Checkbox(checked = isLong, onCheckedChange = {
                viewModel.setIsLong(it)
            })
        }
    }


    /*
    @Composable
    private fun IsPriestSettings() {
        WideRow {
            Text(text = "I am a priest")

            val isPriest by viewModel.optionRepo.isPriest().collectAsState(initial = false)
            Checkbox(checked = isPriest, onCheckedChange = {
                viewModel.setIsPriest(it)
            })
        }
    }
     */

    @Composable
    private fun IsPriestPresentSettings() {
        WideRow {
            Text(text = "A priest is present")

            val isPriestPresent by viewModel.optionRepo.isPriestPresent().collectAsState(initial = false)
            Checkbox(checked = isPriestPresent, onCheckedChange = {
                viewModel.setIsPriestPresent(it)
            })
        }
    }

    @Composable
    private fun DateSetting() {
        val dialog = remember { MaterialDialog() }
        dialog.build {
            datepicker {
                viewModel.setDate(it)
            }
            buttons {
                positiveButton("Select")
                negativeButton("Cancel")
            }
        }

        WideButton(onClick = { dialog.show() }) {
            WideRow(horizontalArrangement = Arrangement.SpaceBetween) {
                Text(text = "Date")

                val date by viewModel.optionRepo.date().collectAsState(initial = "yyyy-MM-dd")
                Text(text = "$date")
            }
        }
    }

    @Composable
    private fun LanguageSettings() {
        var isExpanded by remember{ mutableStateOf(false) }
        val selectedLanguage by viewModel.optionRepo.language().collectAsState(initial = "Language")
        val languages by viewModel.orologionRepo.allLanguages().collectAsState(initial = emptyList())

        WideButton(onClick = { isExpanded = !isExpanded }) {
            WideRow {
                Text(text = "Language")
                Text(text = "$selectedLanguage")

            }
            DropdownMenu(expanded = isExpanded, onDismissRequest = { isExpanded = !isExpanded }) {
                for (language in languages) {
                    DropdownMenuItem(onClick = {
                        viewModel.setLanguage(language)
                        isExpanded = !isExpanded
                    }) { WideText(text = language) }
                }
            }
        }

    }

    @Composable
    private fun ThemeSettings() {
        var isExpanded by remember{ mutableStateOf(false) }
        val selectedTheme by viewModel.optionRepo.theme().collectAsState(initial = "paper")

        WideButton(onClick = { isExpanded = !isExpanded }) {
            WideRow {
                Text(text = "Theme")
                Text(text = "$selectedTheme")
            }
            DropdownMenu(expanded = isExpanded, onDismissRequest = { isExpanded = !isExpanded }) {
                for (theme in Themes.values()) {
                    DropdownMenuItem(onClick = {
                        viewModel.setTheme(theme.toString().toLowerCase())
                        isExpanded = !isExpanded
                    }) { WideText(text = theme.toString().capitalize()) }
                }
            }
        }

    }
}
