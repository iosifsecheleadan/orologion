package com.sechelea.orologion.view.screen

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Settings
import androidx.compose.ui.graphics.vector.ImageVector

enum class SideNavDrawerItems(val icon: ImageVector) {
    Home        (Icons.Default.Home),
    Settings    (Icons.Default.Settings),
    Services    (Icons.Default.List)
}

