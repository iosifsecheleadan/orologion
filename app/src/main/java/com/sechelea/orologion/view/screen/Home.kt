package com.sechelea.orologion.view.screen

import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.*
import androidx.navigation.NavController
import com.sechelea.orologion.viewmodel.OrologionViewModel
import androidx.navigation.compose.navigate
import com.sechelea.orologion.view.custom.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class Home(
    navController: NavController,
    private val viewModel: OrologionViewModel
) : Screen(navController) {

    @Composable
    override fun Show() {
        val scaffoldState = rememberScaffoldState(rememberDrawerState(initialValue = DrawerValue.Closed))
        val scaffoldScope = rememberCoroutineScope()
        Scaffold(
            topBar = { TopBar(scaffoldState, scaffoldScope, "Home") },
            drawerContent = { SideDrawer(scaffoldState, scaffoldScope, navController) },
            scaffoldState = scaffoldState,
        ) {
            Content()
        }
    }

    @Composable
    private fun Content() {
        ScrollableWideColumn {
            val date by viewModel.optionRepo.date().collectAsState(initial = "yyyy-MM-dd")
            val day by viewModel.optionRepo.dayOfWeek().collectAsState(initial = "day")
            val language by viewModel.optionRepo.language().collectAsState(initial = "Language")

            WideRow {
                Text(text = "$day / $date")
                Text(text = "$language")
            }

            WideText(text = if (day == "sun") "Sunday is a day of obligation."
                        else "Days of obligation (coming soon)")
            WideText(text = "Today's feast (coming soon)")
            WideText(text = "Today's fasting prescriptions (coming soon)")
        }
    }
}
