package com.sechelea.orologion.view.screen

import androidx.compose.runtime.Composable
import androidx.navigation.NavController

abstract class Screen (
    protected val navController: NavController
){
    @Composable abstract fun Show()
}