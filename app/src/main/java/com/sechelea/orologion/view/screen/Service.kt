package com.sechelea.orologion.view.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Red
import androidx.compose.ui.graphics.Color.Companion.Unspecified
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.style.TextIndent
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.sechelea.orologion.view.custom.*
import com.sechelea.orologion.viewmodel.OrologionViewModel
import kotlinx.coroutines.flow.flow
import androidx.compose.runtime.getValue
import kotlinx.coroutines.delay

class Service(
    navController: NavHostController,
    private val viewModel: OrologionViewModel,
    private val name: String,
) : Screen(navController) {

    @Composable
    override fun Show() {
        Text(text = "Service")
        val scaffoldState = rememberScaffoldState(rememberDrawerState(initialValue = DrawerValue.Closed))
        val scaffoldScope = rememberCoroutineScope()
        Scaffold(
            topBar = { TopBar(scaffoldState, scaffoldScope, name) },
            drawerContent = { SideDrawer(scaffoldState, scaffoldScope, navController) },
            scaffoldState = scaffoldState,
        ) {
            Content()
        }
    }

    @Composable
    private fun Content() {
        val language by viewModel.optionRepo.language().collectAsState(initial = "Language")
        viewModel.orologionRepo.useLanguage(language ?: "LL")
        val service by viewModel.orologionRepo.serviceText(name).collectAsState(initial = emptyList())

        val state = rememberLazyListState()
        val progress by progressFlow(service.size, state)
            .collectAsState(initial = progress(service.size, state))

        Column {
            LinearProgressIndicator(
                modifier = Modifier.fillMaxWidth(),
                progress = progress
            )

            LazyColumn(
                modifier = Modifier.fillMaxWidth(),
                state = state
            ) {
                service.forEach{ prayer -> item {
                    PrayerCard(lines = prayer)
                } }
            }
        }
    }

    private fun progressFlow(size: Int, state: LazyListState) = flow {
        emit(progress(size, state))
        while (true) {
            delay(100)
            emit(progress(size, state))
        }
    }

    private fun progress(size: Int, state: LazyListState) =
        try {
            val number = state.firstVisibleItemIndex.toFloat() / size.toFloat()
            if (number.isNaN()) 0.0f else number
        } catch (e: ArithmeticException) { 0.0f }


    @Composable
    private fun PrayerCard(lines: List<String>) {
        WideCard { WideColumn {
            lines.forEach{ line ->
                val isRed = line.startsWith("#")
                WideColoredText(
                    text = (if (isRed) line.substring(1).trim() else line.trim()),
                    color = if (isRed) Red else Unspecified,
                    fontStyle = if (isRed) FontStyle.Italic else null,
                    style = LocalTextStyle.current +
                            TextStyle(textIndent = TextIndent(firstLine = 32.sp)),
                )
            }
        } }
    }
}

