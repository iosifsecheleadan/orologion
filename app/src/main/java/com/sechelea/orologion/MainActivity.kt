package com.sechelea.orologion

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.*
import com.sechelea.orologion.viewmodel.OrologionViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHost
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.rememberNavController
import com.sechelea.orologion.view.custom.ThemedBackground
import com.sechelea.orologion.view.screen.Home
import com.sechelea.orologion.view.screen.Service
import com.sechelea.orologion.view.screen.Services
import com.sechelea.orologion.view.screen.Settings

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent { App() }
    }
}

@Composable
fun App(
    viewModel: OrologionViewModel = viewModel()
) {
    val theme by viewModel.optionRepo.theme().collectAsState(initial = "paper")

    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "home") {
        composable("home") { ThemedBackground(theme) {
            Home(navController, viewModel).Show()
        } }

        composable("settings") { ThemedBackground(theme) {
            Settings(navController, viewModel).Show()
        } }

        composable("services") { ThemedBackground(theme) {
            Services(navController, viewModel).Show()
        } }

        composable("service/{name}", arguments = listOf(
            navArgument("name") { type = NavType.StringType }
        )) { ThemedBackground(theme) {
            val name = it.arguments?.getString("name")!!
            Service(navController, viewModel, name).Show()
        } }

        // todo data
        //  - more prayers
        //  - diacritice
        //  - correct text
        //  - limbaj catolic (duh -> spirit, etc) (cu orologhionul catolic)
        //  - imparte toate rugaciunile pe versuri (cu orologhionul catolic)
    }
}
