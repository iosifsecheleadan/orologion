package com.sechelea.orologion.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.sechelea.orologion.model.db.OrologionRoomDataBase
import com.sechelea.orologion.model.repo.OptionRepository
import com.sechelea.orologion.model.repo.OrologionRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import java.time.LocalDate

class OrologionViewModel(
    application: Application
) : AndroidViewModel(application) {

    companion object {
        private const val DELAY_MS : Long = 1000
    }

    val optionRepo by lazy { OptionRepository(
        OrologionRoomDataBase.getDataBase(application).optionDao()
    ) }

    val orologionRepo = OrologionRepository(application.assets, optionRepo)

    init {
        viewModelScope.launch { optionRepo.setDefaultOptions() }
    }

    private fun isOnline() : Boolean{
        val connectivityManager = getApplication<Application>().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork) != null
    }

    fun online() = flow {
        while (true) {
            delay(DELAY_MS)
            emit(isOnline())
        }
    }

    fun setTheme(string: String) = viewModelScope.launch { optionRepo.setTheme(string) }
    fun setLanguage(string: String) = viewModelScope.launch { optionRepo.setLanguage(string) }
    fun setDate(date: LocalDate = LocalDate.now()) = viewModelScope.launch { optionRepo.setDate(date) }
    // fun setIsPriest(boolean: Boolean) = viewModelScope.launch { optionRepo.setIsPriest(boolean) }
    fun setIsPriestPresent(boolean: Boolean) = viewModelScope.launch { optionRepo.setIsPriestPresent(boolean) }
    fun setIsLong(boolean: Boolean) = viewModelScope.launch { optionRepo.setIsLong(boolean) }

}