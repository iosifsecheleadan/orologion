# Prayer File Format

This text explains how to format a prayer file. 

```
Prayer number 1.

Prayer number 2, after a space
Continuation of prayer number 2, on a new line.

# some comments that will not be taken into account

# though the spaces in between will be counted
#
# So comment the lines between comments, to not have too many spaces

# Will search for this file as follows:
# texts-[language]/common/redirect_file.txt
# texts-[language]/[service]/redirect_file.txt
# where language is the currently selected language code (ex.: "RO")
# and service is the currently selected service (ex.: "dimineata")
# This line will be replaced with the contents of that file. 
# That file will be in the same format
@redirect_file

if
some_condition Prayer number 3
other_condition Prayer number 4
new_condition @prayer_number_5
else @default_prayer
end-if
```

Let's break it down. 

## Normal Text

For the program to interpret the prayers, you first need text. 
For this, just write down the text of a prayer.

```
Let's pretend this is a somewhat short prayer that is written here, at this point in the documentation.
```

If your prayer is in verses, simply put each verse on a new line. This is useful for psalms.

```
I'm no psalmist, but 
Let's pretend this is 
a somewhat short prayer 
that is written here, 
at this point in the documentation, 
and also, 
it's in verses.
```

The output of these looks exactly like the input files.

## Red Text 

To have some text appear in red, prefix it with #

```
# This text is red
```

## Spacing

To separate two prayers, leave one empty line between them.

```
Let's pretend this is a somewhat short prayer that is written here, at this point in the documentation.

Now continue to pretend like this were a second prayer.
```

The output of the spaces is kept exactly like in the input files.

## Comments

If you want to note some things for later in your prayer files, that should not be taken into account in the output, you can write some comments.

```
// This is a note for later that will not appear in the output.
And this is a prayer that will.
```

The output of this looks like 

```
And this is a prayer that will.
```

Keep in mind that spaces are still accounted for, even between comments. To avoid unnecessary spaces you can comment out the lines in between.

```
Text 
// This is a comment.
// 
// 
// 
// 
// And this is also a comment. No spaces will appear in between in the output


// But two spaces will appear here for no reason.
Text
```

Output:

```
Text


Text
```

## Reference other files

To use the content of another prayer file, link to it like this

```
@redirect_file
```

This will replace that line with the contents of the file located at: 

- `texts-[language]/common/redirect_file.txt`
- `texts-[language]/[service]/redirect_file.txt`

Where `[language]` is the selected language, and `[service]` is the selected service.

These files should also be formatted in the same manner because they will be interpreted as such.


## If else statements

Since complex prayers have moving parts, if-else-statements can be used to control for some built in parameters.

```
if
is_noapte Prayer for night time
else Prayer for day time
endif
```

The first valid option is selected. If there is an else statement and nothing else before it is valid, it is always selected.

```
if
MON @monday
TUE @tuesday
WED @wednesday
THU @thursday
FRI @friday
SAT @saturday
SUN @sunday
endif
```

For a full list of all if conditions, see [options](options.md).