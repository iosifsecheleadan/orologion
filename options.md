# Options

Specify these in the prompt, after the service you want to see, like

```bash 
~ dimineata lang=RO is_priest=true other=option
```

## Parameter options  

- lang : Two letter code for language
  - RO : romanian language
    
    Default = RO

- date : YYYY-MM-DD string
    
    Default = today's date. 

    Changing this also changes the day of the week option.

    See ***true / false options > mon / tue / wed / thu / fri / sat / sun***

## true / false options (if conditions) 

- is_priest : If the reader is a priest

    Default = false

- is_priest_present : If there is a priest present

    Default = false

- is_long : If it is a long format

    Default = false

- is_night : If it is nighttime

    Default = false

- mon / tue / wed / thu / fri / sat / sun : If it is the specified day of the week

    Default = today's day of the week. 
    
    This can be changed manually, but it should be changed through the date option. 
    
    See ***Parameter options > date***

