# Orologion

An Android application to simplify praying the Byzantine Liturgy of the Hours.

## Install

Follow these steps on your Android phone: 

1. Download the Android APK from [here](https://gitlab.com/iosifsecheleadan/orologion/-/raw/master/app-release.apk). If your preferred browser downloads a .zip file instead of an .apk file, try using the native browser.

2. Navigate to the download folder.

3. Tap the downloaded file.

4. Allow installing from Browser as "unknown source".

5. Tap "Install". If Play Protect or some other software tries to impede the install, tap "Install Anyway".

## Application Interface

### Home

At the top of the Home screen one can see the selected date and the selected language.

![home screen ss](presentation/home.png)

### Side Navigation Tab

Navigate through the app using the Side Navigation Tab. 

![side nav tab ss](presentation/side-nav-tab.png)

### Settings

In the Settings Screen, you can 
- switch between long and short form prayers
- specify if a priest is present (this changes some prayers)
- select a different date
- change the language
- change the theme

![settings screen ss](presentation/settings.png)

### Languages

Currently available languages: 
- Romana

### Themes

Currently available themes:
- Paper (default)
- Light
- Dark

### Services

Select what Service to pray from the Services Screen.

![services screen ss](presentation/services.png)

### Service Sample

Each Service consists of multiple prayers, with some occasional notes before or after them. At the top you can see a progress bar, indicating how much of the service you've gone through. 

![service sample ss](presentation/service-sample.png)